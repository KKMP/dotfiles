$env:path = "$env:userprofile\scoop\shims\;$env:path"
$env:path = "$env:userprofile\.bin\;$env:path"

# load sql helper
Import-Module "$PSScriptRoot\sql_helpers.psm1" -Force


if (Get-Module -ListAvailable -Name ActiveDirectory)
{
    $dnsName = "nclan.netcompany.dk"
    try
    {
        $dnsRecord = Resolve-DnsName -Name $dnsName -ErrorAction Stop
        if ($dnsRecord.Name -eq $dnsName)
        {
            $adServer = (Get-ADDomain).DNSRoot
        } else
        {
            $adServer = (Get-ADDomain).DNSRoot
        }
    } catch
    {
        Write-Host "DNS record $dnsName does not exist (catch)"
    }

} else
{
    Write-Host "Try install: 'RSAT-AD-PowerShell' for active directory stuff. 'Add-WindowsCapability –online –Name Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0'"
}


Function Mount-Ncop
{
    ssh -L 636:localhost:636 kkmp@ncop-ldpd-add01.dev.ldp.ncop.nchosting.dk -J kkmp@ssh.nchosting.dk
    # ssh -L [LocalPort]:127.0.0.1:55555 [VaultUser]@[TargetUser]@[TargetAddress]#22#[TargetPort]@pam-ssh.nchosting.dk
}
Function ldev
{Set-Location -Path "\\wsl$\Ubuntu\home\kkmp\dev\"
}
Function dev
{Set-Location -Path ~/dev/
}
Function mvc
{Set-Location -Path ~/dev/mvc_dev/MVCPortal/MVCPortal/
}
Function mvcd
{Set-Location -Path ~/dev/ad-portal/
}
Function adportal
{Set-Location -Path ~/dev/adportalmodule/
}
Function ser
{Set-Location -Path ~/dev/server-portal/
}
Function sql
{Set-Location -Path ~/dev/sql-portal/
}
Function fire
{Set-Location -Path ~/dev/firewall-portal/
}
Function pmain
{Set-Location -Path ~/dev/main-portal/
}
Function dotfiles
{Set-Location -Path ~/dev/dotfiles
}

# Active Direcotry stuff
Function GAR([string] $name)
{Get-ADGroup -Properties Members $name -Server $adServer
}
Function GAM([string] $name)
{Get-ADGroup -Properties MemberOf $name -Server $adServer
}
Function UM([string] $name)
{Get-ADUser -Properties MemberOf $name -Server $adServer
}

Function G([string] $name)
{
    Get-ADGroup -Server $adServer -Identity $name
}

Function U
{
    param(
        [Parameter(Position=0)]
        [string]$Identity,
        [string]$Filter,
        [string]$LDAPFilter
    )
    $parameters = @{
        'Server' = $adServer
        'Properties' = 'EmailAddress',
        'EmployeeId',
        'EmployeeType',
        'WhenChanged',
        'WhenCreated',
        'ObjectGuid',
        'OfficePhone',
        'Company',
        'Office',
        'Department',
        'Title',
        'Description'
    }

    if ($Identity)
    {
        $parameters['Identity'] = $Identity
    }

    if ($Filter)
    {
        $parameters['Filter'] = $Filter
    }

    if ($LDAPFilter)
    {
        $parameters['LDAPFilter'] = $LDAPFilter
    }

    Get-ADUser @parameters
}


if ($alias:curl)
{
    Remove-Item alias:curl
}

if ($alias:mv)
{
    Remove-Item alias:mv
}

if ($alias:rm)
{
    Remove-Item alias:rm
}

if ($alias:ls)
{
    Remove-Item alias:ls
    # set-alias ls -Value lsd
    Function ll
    {
        &"ls -lha"
    }
}

function Export-NetworkProfileEvents {
    param (
        [string]$Date = (Get-Date).ToString('dd-MM-yyyy')
    )

    $logName = 'Microsoft-Windows-NetworkProfile/Operational'
    $eventIDs = @(10000, 10001)
    $dateFilter = [datetime]::ParseExact($Date, 'dd-MM-yyyy', $null)
    $endDate = $dateFilter.AddDays(1)

    $events = Get-WinEvent -LogName $logName |
              Where-Object { ($_.Id -eq 10000 -or $_.Id -eq 10001) -and $_.TimeCreated -ge $dateFilter -and $_.TimeCreated -lt $endDate } |
              Select-Object TimeCreated, Id, Message |
              Sort-Object -Property TimeCreated

    $desktopPath = [System.IO.Path]::Combine([System.Environment]::GetFolderPath('Desktop'), 'NetworkProfileEvents.csv')
    Write-Host "Found the following events:"
    $events | Format-Table
    $events | Export-Csv -Path $desktopPath -NoTypeInformation

    Write-Output "Events exported to $desktopPath"
}


$DOTNET_CLI_TELEMETRY_OPTOUT=1

function co
{
    param(
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [ArgumentCompleter({
                param($pCmd, $pParam, $pWord, $pAst, $pFakes)

                $branchList = (git branch --format='%(refname:short)')

                if ([string]::IsNullOrWhiteSpace($pWord))
                {
                    return $branchList;
                }

                $branchList | Select-String "$pWord"
            })]
        [string] $branch
    )

    git checkout $branch;
}

function Test-Password
{
    param(
        [string]$Username,
        [string]$Password
    )

    try
    {
        $securePass = ConvertTo-SecureString "$Password" -AsPlainText -Force
    } catch
    {
        return
    }
    $cred = New-Object System.Management.Automation.PSCredential($Username, $securePass)

    try
    {
        # Attempt to authenticate
        $null = Get-ADUser -Credential $cred -Filter *
    } catch
    {
        Write-Host $_
    }
}

# Function to check account lockout status
function Get-AccountLockoutStatus
{
    param(
        [string]$Username
    )

    $user = Get-ADUser $Username -Properties LockedOut, AccountLockoutTime
    Write-Host "`nAccount Status for $Username :"
    Write-Host "Locked Out: $($user.LockedOut)"
    Write-Host "Lockout Time: $($user.AccountLockoutTime)"
}

function Test-BadPassword
{
    param(
        [string]$Username,
        [int]$Attempts = 5
    )

    Write-Host "Starting bad password attempts for user: $Username"

    for ($i = 1; $i -le $Attempts; $i++)
    {
        $securePass = ConvertTo-SecureString "WrongPassword123!" -AsPlainText -Force
        $cred = New-Object System.Management.Automation.PSCredential($Username, $securePass)

        try
        {
            # Attempt to authenticate
            $null = Get-ADUser -Credential $cred -Filter *
        } catch
        {
            Write-Host "Failed attempt $i of $Attempts"
        }
    }
}


Function Get-TlsCertificate
{

    [CmdletBinding()]

    Param (

        [Parameter(Mandatory)]
        [string]$Hostname,
        [int]$Port = 443,
        [Alias('Path', 'FilePath')]
        [string]$OutFile

    )

    $Certificate = $Null

    $TcpClient = New-Object -TypeName System.Net.Sockets.TcpClient

    Try
    {

        Write-Verbose "Connecting to $Hostname on port $Port..."
        $TcpClient.Connect($Hostname, $Port)
        $TcpStream = $TcpClient.GetStream()

        $Callback = {

            Param (

            )

            Return $True

        }

        $SslStream = New-Object -TypeName System.Net.Security.SslStream -ArgumentList @($TcpStream, $True, $Callback)

        Try
        {

            Write-Verbose 'Retrieving TLS certificate...'
            $SslStream.AuthenticateAsClient($Hostname)
            $Certificate = $SslStream.RemoteCertificate

        }

        Finally
        {

            $SslStream.Dispose()

        }

    }

    Finally
    {

        $TcpClient.Dispose()

    }

    If ($Certificate)
    {

        If ($Certificate -IsNot [System.Security.Cryptography.X509Certificates.X509Certificate2])
        {

            Write-Verbose 'Converting certificate to X509Certificate2 object...'
            $Certificate = New-Object -TypeName System.Security.Cryptography.X509Certificates.X509Certificate2 -ArgumentList $Certificate

        }

        # Display certificate details
        Write-Output $Certificate.ToString()

    }

    If ($OutFile)
    {

        Write-Verbose "Saving certificate to $OutFile..."
        $CertOut = New-Object System.Text.StringBuilder
        $CertOut.AppendLine("-----BEGIN CERTIFICATE-----") | Out-Null
        $CertOut.AppendLine([System.Convert]::ToBase64String($Certificate.RawData, 1)) | Out-Null
        $CertOut.AppendLine("-----END CERTIFICATE-----") | Out-Null
        $CertOut.ToString() | Out-File $OutFile | Out-Null

    }

}

Function Repair-WSLNetwork
{
    Get-NetAdapter | Where-Object {$_.InterfaceDescription -Like 'Cisco AnyConnect*'} | Set-NetIPInterface -InterfaceMetric 6000
}

Function Find-History
{
    $find = $args;
    Write-Host "Finding in full history using {`$_ -like `"*$find*`"}";
    Get-Content (Get-PSReadlineOption).HistorySavePath | Where-Object {$_ -like "*$find*"} | Get-Unique | more
}

Function New-Shortcut
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [System.IO.FileInfo]$Source,
        [string]$Arguments,
        [Parameter(Mandatory)]
        [System.IO.FileInfo]$DestinationPath
    )

    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut($DestinationPath.FullName)
    $Shortcut.TargetPath = $Source.FullName
    if ($Arguments)
    {
        $Shortcut.Arguments = $Arguments
    }
    $Shortcut.Save()
}

# PowerShell script to prevent sleep
$null = Add-Type -TypeDefinition @"
using System;
using System.Runtime.InteropServices;

public static class PowerManagement {
   [DllImport("kernel32.dll", SetLastError = true)]
   public static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);
}

public enum EXECUTION_STATE : uint {
   ES_AWAYMODE_REQUIRED = 0x00000040,
   ES_CONTINUOUS = 0x80000000,
   ES_DISPLAY_REQUIRED = 0x00000002,
   ES_SYSTEM_REQUIRED = 0x00000001
}
"@

function Start-DontGoToSleep
{
    # Prevent sleep
    [PowerManagement]::SetThreadExecutionState([EXECUTION_STATE]::ES_CONTINUOUS -bor [EXECUTION_STATE]::ES_SYSTEM_REQUIRED)

    # Keep the script running until you decide to stop it
    Read-Host "Press Enter to exit and allow sleep mode again"
    [PowerManagement]::SetThreadExecutionState([EXECUTION_STATE]::ES_CONTINUOUS)
}


# For editing Microsoft SQL alias and stuff
Function Edit-SQL-Alias
{
    C:\Windows\SysWOW64\SQLServerManager15.msc
}

# for editing your PowerShell profile
Function Edit-Profile
{
    nvim $PROFILE
}

Function Edit-Starship
{
    nvim $home\.config\starship.toml
}

# for editing dot files
Function Edit-Dotfiles
{
    Set-Location ~/dev/dotfiles
}

# for editing your Vim settings
Function Edit-Vimrc
{
    vim $home\_vimrc
}

Function Edit-Neovim
{
    vim $home\AppData/Local/nvim/init.vim
}

Function Edit-Nu
{
    vim $home\AppData\Roaming\nushell\nu\config\config.toml
}

function Open-Toolkit (
    [string] $UnparsedTypeAndId
)
{
    function parseTypeAndId ([string] $unparsedTypeAndId)
    {
        $id = $unparsedTypeAndId.Split('_')[0]

        if($id.StartsWith("W", 'CurrentCultureIgnoreCase'))
        {
            return $("WorkPackages", $id.SubString(1))
        }
        # Returns a tuple
        return $("Tasks", $id) # Return a tuple
    }

    if(!$UnparsedTypeAndId)
    {
        $UnparsedTypeAndId = git branch --show-current
        if (!$?)
        {
            throw "The command did not receive any input or it is not in any valid git repo"
        }
    }

    # Destructuring the tuple here
    $type, $id = parseTypeAndId($UnparsedTypeAndId)
    $link = "https://goto.netcompany.com/cases/GTO399/NCOBAD/Lists/$type/DispForm.aspx?ID=$id"

    Write-Output $link
    Start-Process $link
}


function Add-DeployAccount
{
    [CmdletBinding()]
    param ()

    Write-Host (Get-Location)
    $null = Read-Host "Press enter if the above location is correct else Ctrl+C"

    $cred = Get-Credential -UserName "deployAccount"

    $passwordBytes = [System.Text.Encoding]::Unicode.GetBytes($cred.GetNetworkCredential().Password)
    $encryptedPassword = [System.Security.Cryptography.ProtectedData]::Protect($passwordBytes, $null, [System.Security.Cryptography.DataProtectionScope]::CurrentUser)

    New-Item -Type Directory DeployAccounts\ -ErrorAction Ignore

    Set-Content DeployAccounts\B5C41083-67F7-4A27-AFCB-8E7CC64D35A1 -AsByteStream -Value $encryptedPassword
    Set-Content DeployAccounts\1A5A14D6-67ED-4543-95E5-1D55CEBB9C7B -AsByteStream -Value $encryptedPassword
}


Enum Scope
{
    CurrentUser
    Machine
}

Add-Type -AssemblyName System.Security -ErrorAction Continue
Function Get-PasswordCrypt
{
    [CmdletBinding()]
    param (
        [System.Security.Cryptography.DataProtectionScope]$Scope = [System.Security.Cryptography.DataProtectionScope]::CurrentUser
    )
    $securePassword = Read-Host -Prompt "Enter your password" -AsSecureString
    $password = [System.Net.NetworkCredential]::new("", $securePassword).Password
    $passwordBytes = [System.Text.Encoding]::Unicode.GetBytes($password)
    $encryptedPassword = [System.Security.Cryptography.ProtectedData]::Protect($passwordBytes, $null, $Scope)
    $base64Password = [System.Convert]::ToBase64String($encryptedPassword)
    $base64Password
}

function Add-PasswordFile
{
    [CmdletBinding()]
    param (
        [parameter(mandatory=$true)][string]$file,
        [Scope]$Scope
    )
    $securePassword = Read-Host -Prompt "Enter your password" -AsSecureString
    Add-Type -AssemblyName System.Security

    switch($Scope)
    {
        Scope::Machine
        {
            $Scope = [System.Security.Cryptography.DataProtectionScope]::LocalMachine
        }
        Scope::CurrentUser
        {
            $Scope = [System.Security.Cryptography.DataProtectionScope]::CurrentUser
        }
        Default
        {
            $Scope = [System.Security.Cryptography.DataProtectionScope]::CurrentUser
        }
    }

    $password = [System.Net.NetworkCredential]::new("", $securePassword).Password
    $passwordBytes = [System.Text.Encoding]::Unicode.GetBytes($password)
    $encryptedPassword = [System.Security.Cryptography.ProtectedData]::Protect($passwordBytes, $null, $Scope)

    Set-Content $file -Value $encryptedPassword -Encoding Byte
}

Function EncryptPasswordTo-Base64
{
    $password = Read-Host -Prompt "Enter your password" -AsSecureString
    $encryptedPassword = $password | ConvertFrom-SecureString

    $encryptedBytes = [System.Text.Encoding]::Unicode.GetBytes($encryptedPassword)
    $base64Encoded = [Convert]::ToBase64String($encryptedBytes)

    $base64Encoded
}


function Get-Assembly
{
    [CmdLetBinding()]
    [OutPutType([PSCustomObject])]
    param(
        [Parameter(Mandatory)]
        [string] $Path
    )
    $file = Get-Item -Path $Path
    $fullPath = $file.VersionInfo.FileName
    Write-Output $fullPath

    return [system.reflection.assembly]::loadfile($fullPath)
}

function Get-PublicTokenFromDll
{
    [CmdLetBinding()]
    [OutPutType([PSCustomObject])]
    param(
        [Parameter(Mandatory)]
        [string] $Path
    )
    return (Get-Assembly -Path $Path).FullName
}

function Get-PublicTokenFromDll1
{
    [CmdLetBinding()]
    [OutPutType([PSCustomObject])]
    param(
        [Parameter(Mandatory)]
        [string] $FilePath
    )
    ([system.reflection.assembly]::loadfile($FilePath)).FullName
}


if (Get-Module -ListAvailable -Name Posh-Git)
{
    Import-Module posh-git
    $GitPromptSettings.EnableFileStatus = $false
}


Set-PSReadLineOption -EditMode vi
Set-PSReadlineKeyHandler -Chord Tab -Function Complete
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete
Set-PSReadlineOption -ShowToolTips -BellStyle Visual
Set-PSReadLineKeyHandler -Chord Ctrl+o -ScriptBlock {
    lfcd
    [Microsoft.PowerShell.PSConsoleReadLine]::Insert('cls;')
    [Microsoft.PowerShell.PSConsoleReadLine]::AcceptLine()
    [Microsoft.PowerShell.PSConsoleReadLine]::Insert('ls;')
    [Microsoft.PowerShell.PSConsoleReadLine]::AcceptLine()
}

try
{
    Invoke-Expression (&starship init powershell)
} Catch
{
}
