# Check if the SqlServer module is loaded, if not, try to import it
if (-not (Get-Module -Name SQLPS)) {
    try {
        Import-Module -Name SQLPS -ErrorAction Stop
    } catch {
        Write-Host "The SQLPS module is not available. Please ensure it is installed and try again."
        exit
    }
}

$backupPath = "C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\Backup\AD"

function Get-SqlServerInstance {
    return "$(hostname)\SQLEXPRESS"
}

function Get-Dbs {
    param (
        [string]$ServerInstance = (Get-SqlServerInstance)
    )

    $query = @"
SELECT name
FROM master.sys.databases
WHERE name not in ('master', 'tempdb', 'model', 'msdb')
"@

    $result = Invoke-Sqlcmd -ServerInstance $ServerInstance -Query $query

    return $result.name
}


# Function to close existing connections to the database
function Close-ExistingConnections {
    param (
        [string]$ServerInstance = (Get-SqlServerInstance),
        [ArgumentCompleter({Get-Dbs})]
        [Parameter(Mandatory)]
        [string]$DatabaseName
    )
    Write-Host "Start: close existing connections for '$DatabaseName' on '$ServerInstance'"

#     $query = @"
# USE master;
# ALTER DATABASE [$DatabaseName] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
# "@
#     Invoke-Sqlcmd -ServerInstance $ServerInstance -Query $query
    Invoke-Sqlcmd -ServerInstance $ServerInstance -ErrorAction Stop `
        -Query "ALTER DATABASE [$DatabaseName] SET OFFLINE WITH ROLLBACK IMMEDIATE"

    Invoke-Sqlcmd -ServerInstance $ServerInstance -Query "ALTER DATABASE [$DatabaseName] SET ONLINE"

    Write-Host "Done: close existing connections for '$DatabaseName' on '$ServerInstance'"
}

# Function to get the latest backup set
function Get-DatabaseLatestBackupSet {
    param (
        [string]$BackupPath = $backupPath
    )

    $latestGroup = Get-ChildItem -Path $BackupPath -Filter "PortalDB_*_Full_*.bak" |
                   Group-Object { $_.Name -replace 'PortalDB_\d+_Full_(.+)\.bak', '$1' } |
                   Sort-Object Name -Descending |
                   Select-Object -First 1
    return $latestGroup.Name
}

function Test-DatabaseExists {
    param (
        [string]$ServerInstance = (Get-SqlServerInstance),
        [string]$DatabaseName
    )

    $query = "SELECT CASE WHEN DB_ID('$DatabaseName') IS NOT NULL THEN 1 ELSE 0 END AS DatabaseExists;"
    $result = Invoke-Sqlcmd -ServerInstance $ServerInstance -Query $query
    return $result.DatabaseExists -eq 1
}


function Get-DatabaseBackupSet {
    param(
        [string]$BackupPath = $backupPath
    )

    $backupSets = Get-ChildItem -Path $BackupPath -Filter "PortalDB_*_Full_*.bak" |
                  Group-Object { $_.Name -replace 'PortalDB_\d+_Full_(.+)\.bak', '$1' } |
                  Sort-Object Name -Descending |
                  Select-Object -ExpandProperty Name

    return $backupSets
}


# Function to restore the database
function Restore-DB {
    param (
        [switch]$Latest,
        [string]$BackupPath = $backupPath,

        [ArgumentCompleter({Get-DatabaseBackupSet})]
        [string]$BackupName,

        [string]$ServerInstance = (Get-SqlServerInstance),
        [ArgumentCompleter({Get-Dbs})]
        [string]$DatabaseName = "Dev_AD"
    )

    process {
        # Determine the backup set to use
        if ($Latest) {
            $backupSet = Get-DatabaseLatestBackupSet -BackupPath $BackupPath
        } elseif ($BackupName) {
            $backupSet = $BackupName
        } else {
            Write-Host "Please specify either -Latest or -Name parameter."
            break
        }

        # Get the backup files
        $backupFiles = Get-ChildItem -Path $BackupPath -Filter "PortalDB_*_Full_$backupSet.bak" | Sort-Object Name

        if ($backupFiles.Count -eq 0) {
            Write-Host "No backup files found for the specified backup set: $backupSet"
            break
        }

        # Check if the database exists
        $databaseExists = Test-DatabaseExists -SqlInstance $ServerInstance -Database $DatabaseName

        if ($databaseExists) {
            Write-Host "Database $DatabaseName already exists. Closing existing connections"

            # Close existing connections
            Close-ExistingConnections -ServerInstance $ServerInstance -DatabaseName $DatabaseName
        }

        # Restore the database from the backup files
        $backupFilesArray = $backupFiles.FullName
        $backupFilesArray | ForEach-Object { Write-Host $_ }

        Write-Host "Restore database '$DatabaseName' on '$ServerInstance'"
        Restore-SqlDatabase -ServerInstance $ServerInstance `
           -Database $DatabaseName  `
           -ReplaceDatabase `
           -BackupFile $backupFilesArray `
           -ErrorAction Stop

        # Reset the database to MULTI_USER mode
        $query = @"
USE master;
ALTER DATABASE [$DatabaseName] SET MULTI_USER;
"@

        Write-Host "Set multi user mode on database '$DatabaseName'"
        Invoke-Sqlcmd -ServerInstance $ServerInstance -Query $query

        Write-Host "Database restoration complete."
    }
}
